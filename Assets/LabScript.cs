using System;
using UnityEngine;

[Serializable]
public class LabScript : MonoBehaviour
{
    public float a;
    public float b;
    public Vector3 c;
}
