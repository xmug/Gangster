﻿using Crowd.Models;
using UnityEngine;
using XMUGFramework;

namespace Crowd
{
    public class GameCore : BaseCore<GameCore>
    {
        protected override void Init()
        {
            AutoRegisterAllManagersInInspector();
            Register(new EventSystem());
            Register(new PlayerModel());
            Register(new TeamModel());
            Register(new GameModel());
            Register(new PlayerPrefsStorage());
        }
    }
}
