using UnityEngine;
using XMUGFramework;

namespace  Crowd
{
    public class PrefabToolBox : AManager
    {
        public GameObject build_Prefab;
        public GameObject[] building_prefabs;

        public GameObject streetLight_prefab;

        public GameObject boyPrefab;
        public GameObject enemyPrefab;
        public GameObject enemyBossPrefab;


        public GameObject obstaclePrefab;
        public GameObject[] carPrefab;

        public GameObject getMoneyEffectPrefab;


        public GameObject moneyStuffPrefab;

        public GameObject boyTrailPrefab;


        public GameObject hitEffectPrefab;


        public GameObject bloodEffectPrefab;


        public GameObject chicken_stuff_prefab;

        public GameObject playersReachedLine_prefab;


        public GameObject GetBuildingPrefab()
        {
            GameObject buildingPrefab =
                GameObject.Instantiate(building_prefabs[Random.Range(0, building_prefabs.Length)]);
            return buildingPrefab;
        }
    }
    
}
