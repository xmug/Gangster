﻿using Crowd.ModelEvents;
using Crowd.Models;
using UnityEngine;
using UnityEngine.SocialPlatforms;
using XMUGFramework;

namespace Crowd
{
    public class SpawnerManager : AManager
    {
        public float timer;
        public float moneyTimer;
        public float enemyBossTimer;
        public float stuffTimer;
        public float waveEnemiesCount;
        public float waveCarCount;
        public float spawnMoneyInterval;
        public PlayerController defaultPlayer;
        private GameObject playersRoot;
        private GameObject otherStuffRoot;
        private GameObject enemyRoot;
        public float playerRunReachedDistance;

        private UIManager uiManager;
        private PlayerModel playerModel;
        private GameModel gameModel;
        private TeamModel teamModel;
        private EventSystem eventSystem;
        private PrefabToolBox prefabToolBox;


        private void Awake()
        {
            prefabToolBox = GameCore.Get<PrefabToolBox>();
            teamModel = GameCore.Get<TeamModel>();
            playerModel = GameCore.Get<PlayerModel>();
            gameModel = GameCore.Get<GameModel>();
            eventSystem = GameCore.Get<EventSystem>();

            playerModel.playerDafaultNum.OnValueChanged += i => { SpawnPlayers(); };

            stuffTimer = 5;
            playerRunReachedDistance = 200;
        }

        private void Start()
        {
            // Generate Some roots
            otherStuffRoot = new GameObject();
            otherStuffRoot.transform.parent = this.transform;
            otherStuffRoot.name = "OtherStuffRoot";
            
            playersRoot = new GameObject();
            playersRoot.transform.parent = this.transform;
            playersRoot.name = "PlayerRoot";
            
            enemyRoot = new GameObject();
            enemyRoot.transform.parent = this.transform;
            enemyRoot.name = "EnemyRoot";
            
            SpawnPlayers();
        }

        private void Update()
        {
            if(gameModel.gameState.Value != GameState.GameStart)
                return;


            // spawn obstacle
            timer += Time.deltaTime;
            if (timer > 3f)
            {
                timer = 0;

                float f = Random.Range(0.0f, 1.0f);
                if (f < 0.75f)
                {
                    for (int i = 0; i < teamModel.playerList.Count * Random.Range(0.75f, 0.9f); i++)
                    {
                        int segIndex = 40;
                        GameObject enemy = GameObject.Instantiate(prefabToolBox.enemyPrefab,
                            gameModel.roadCentrerPos_List[segIndex].transform.position, Quaternion.identity);
                        enemy.transform.rotation = Quaternion.LookRotation(
                            gameModel.roadCentrerPos_List[segIndex].transform.position -
                            gameModel.roadCentrerPos_List[segIndex - 1].transform.position);
                        enemy.transform.parent = enemyRoot.transform;
                        enemy.transform.position += new Vector3(Random.Range(-3, 3), 0, Random.Range(-3, 3));
                    }
                }
                else
                {
                    for (int i = 0; i < waveCarCount; i++)
                    {
                        int segIndex = 40;
                        GameObject car = GameObject.Instantiate(
                            prefabToolBox.carPrefab[Random.Range(0, prefabToolBox.carPrefab.Length)],
                            gameModel.roadCentrerPos_List[segIndex].transform.position, Quaternion.identity);
                        car.transform.rotation = Quaternion.LookRotation(
                            gameModel.roadCentrerPos_List[segIndex].transform.position -
                            gameModel.roadCentrerPos_List[segIndex - 1].transform.position);
                        car.transform.parent = otherStuffRoot.transform;
                        car.transform.position += car.transform.forward * Random.Range(-30, 30);
                        car.transform.position += car.transform.right * Random.Range(-3.0f, 3.0f);
                    }
                }
            }

            // spawn money
            moneyTimer += Time.deltaTime;
            if (moneyTimer > spawnMoneyInterval)
            {
                moneyTimer = 0;
                int segIndex = Random.Range(35, 45);
                GameObject money = GameObject.Instantiate(prefabToolBox.moneyStuffPrefab,
                    gameModel.roadCentrerPos_List[segIndex].transform.position + Vector3.up * 1.2f,
                    Quaternion.identity);
                money.transform.parent = otherStuffRoot.transform;
                money.transform.position += new Vector3(Random.Range(-8.0f, 8.0f), 0, Random.Range(-8.0f, 8.0f));
            }

            // spawn boss 
            enemyBossTimer += Time.deltaTime;
            if (enemyBossTimer > 15)
            {
                enemyBossTimer = 0;
                GameObject enemyBoss = GameObject.Instantiate(prefabToolBox.enemyBossPrefab,
                    gameModel.roadCentrerPos_List[40].transform.position + Vector3.up, Quaternion.identity);
                enemyBoss.transform.rotation = Quaternion.LookRotation(
                    gameModel.roadCentrerPos_List[40 - 1].transform.position -
                    gameModel.roadCentrerPos_List[40].transform.position);
                enemyBoss.transform.parent = enemyRoot.transform;
                enemyBoss.transform.position += enemyBoss.transform.forward * Random.Range(-5, 5);
                enemyBoss.transform.position += enemyBoss.transform.right * Random.Range(-3.0f, 3.0f);
            }

            if (playerRunReachedDistance < playerModel.playerCurRunDis)
            {
                GameObject playerReachedLine = GameObject.Instantiate(prefabToolBox.playersReachedLine_prefab,
                    gameModel.roadCentrerPos_List[40].transform.position, Quaternion.identity);
                playerReachedLine.transform.rotation = Quaternion.LookRotation(
                    gameModel.roadCentrerPos_List[40].transform.position -
                    gameModel.roadCentrerPos_List[40 - 1].transform.position);
                playerReachedLine.transform.parent = otherStuffRoot.transform;
                playerReachedLine.GetComponentInChildren<TextMesh>().text =
                    (playerRunReachedDistance * 5).ToString() + " m";

                playerRunReachedDistance += 200;
            }
        }


        public int startPlayersCount;

        public void SpawnPlayers()
        {
            startPlayersCount = playerModel.playerDafaultNum.Value;

            for (int i = 0; i < startPlayersCount; i++)
            {
                GameObject player = GameObject.Instantiate(prefabToolBox.boyPrefab,
                    defaultPlayer.transform.position + new Vector3(Random.Range(-5, 5), 0, Random.Range(-5, 5)),
                    Quaternion.identity);
                player.GetComponent<PlayerController>().inTeam = true;
                player.transform.parent = playersRoot.transform;
                teamModel.playerList.Add(player.GetComponent<PlayerController>());
            }
        }
    }
}