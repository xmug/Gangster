﻿using System;
using System.Collections.Generic;
using Crowd.Models;
using Crowd.SpawnEvents;
using UnityEngine;
using XMUGFramework;
using Random = UnityEngine.Random;

namespace Crowd
{
    public class MapManager : AManager
    {
        public int straightRoadLength;
        public float waveBoysCount;
        public float obstacleCount;

        public Transform roadBeginPos;

        public int roadWidth;
        public int buildingsGroundWidth;
        public int roadSegIndex;

        public Material roadMaterial;
        public Material npcWalkRoadMaterial;
        public Material buildingsGroundMaterial;

        public Transform road_root;

        private PrefabToolBox prefabToolBox;
        private GameModel gameModel;

        private void Awake()
        {
            prefabToolBox = GameCore.Get<PrefabToolBox>();
            gameModel = GameCore.Get<GameModel>();
            
            GameCore.Get<EventSystem>().Register<SpawnMap>(map =>
            {
                SpawnStraightRoad();
                SpawnTurnRoad();
                RemoveRoadBehind();
            });
        }

        private void Start()
        {
            SpawnRoad();
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.KeypadEnter))
            {
                SpawnStraightRoad();
                SpawnTurnRoad();
            }

            if (Input.GetKeyDown(KeyCode.Backspace))
            {
                RemoveRoadBehind();
            }
        }

        [ContextMenu("Spawn Road")]
        private void SpawnRoad()
        {
            // Clear
            var UselessRoads = road_root.GetComponentsInChildren<Transform>();
            foreach(var UselessRoad in UselessRoads)
            {
                if(UselessRoad == road_root.transform) continue;
                DestroyImmediate(UselessRoad.gameObject);
            }
            
            straightRoadLength = 100;
            roadSegIndex = 0;
            
            roadBeginPos.position = new Vector3(0, 0, 0);
            roadBeginPos.rotation = Quaternion.Euler(0, 0, 0);

            for (int i = 0; i < 4; i++)
            {
                SpawnStraightRoad();
                SpawnTurnRoad();
            }
        }


        public void SpawnStraightRoad()
        {
            GameObject straightRoad = new GameObject("straight_road" + roadSegIndex.ToString());
            
            // GroundLayer
            straightRoad.layer = 8;
            
            gameModel.roadStraightSegs_list.Add(straightRoad);

            straightRoad.transform.parent = road_root.transform;
            straightRoad.AddComponent<MeshFilter>();
            straightRoad.AddComponent<MeshRenderer>();
            roadSegIndex++;

            Mesh straightRoadMesh = new Mesh();
            Vector3[] vertices = new Vector3[]
            {
                roadBeginPos.position + roadBeginPos.right * -roadWidth / 2,
                roadBeginPos.position + roadBeginPos.right * roadWidth / 2,
                roadBeginPos.position + roadBeginPos.right * -roadWidth / 2 + roadBeginPos.forward * straightRoadLength,
                roadBeginPos.position + roadBeginPos.right * roadWidth / 2 + roadBeginPos.forward * straightRoadLength,
            };
            int[] triangles = new int[]
            {
                0, 2, 1,
                1, 2, 3
            };

            Vector2[] uvs = new Vector2[]
            {
                new Vector2(0f, 0),
                new Vector2(1f, 0),
                new Vector2(0, straightRoadLength / 10),
                new Vector2(1, straightRoadLength / 10),
            };

            straightRoadMesh.vertices = vertices;
            straightRoadMesh.triangles = triangles;
            straightRoadMesh.uv = uvs;

            straightRoadMesh.RecalculateNormals();
            straightRoad.GetComponent<MeshFilter>().mesh = straightRoadMesh;
            straightRoad.GetComponent<MeshRenderer>().material = roadMaterial;
            straightRoad.AddComponent<MeshCollider>();

            SpawnBoys(roadBeginPos.position, roadBeginPos.forward, straightRoadLength);
            SpawnObstacle(roadBeginPos.position, roadBeginPos.forward, straightRoadLength, straightRoad.transform);


            AddRoadCenterPos(roadBeginPos.position, straightRoad.transform);
            roadBeginPos.position += roadBeginPos.forward * straightRoadLength;
            AddRoadCenterPos(roadBeginPos.position, straightRoad.transform);


            //  straightRoadLength += 5;

            GameObject npcWalkRoad = new GameObject("npcWalkRoad");
            npcWalkRoad.layer = 8;
            npcWalkRoad.transform.parent = straightRoad.transform;

            npcWalkRoad.AddComponent<MeshFilter>();
            npcWalkRoad.AddComponent<MeshRenderer>();

            Mesh npcWalkRoadMesh = new Mesh();

            Vector3[] npcWalkRoadMeshVertices = new Vector3[vertices.Length];
            for (int i = 0; i < vertices.Length / 2; i++)
            {
                npcWalkRoadMeshVertices[i * 2 + 1] = vertices[i * 2];
                npcWalkRoadMeshVertices[i * 2] = vertices[i * 2] + (vertices[i * 2] - vertices[i * 2 + 1]) * 0.3f;
            }

            npcWalkRoadMesh.vertices = npcWalkRoadMeshVertices;
            npcWalkRoadMesh.triangles = triangles;
            npcWalkRoadMesh.uv = uvs;

            npcWalkRoad.GetComponent<MeshFilter>().mesh = npcWalkRoadMesh;
            npcWalkRoad.GetComponent<MeshRenderer>().material = npcWalkRoadMaterial;
            npcWalkRoad.AddComponent<MeshCollider>();

            //SpawnSideColliders(npcWalkRoad.transform, npcWalkRoadMeshVertices, triangles, uvs);
            SpawnTurnSideCollider(npcWalkRoad.transform, npcWalkRoadMeshVertices, triangles, uvs, true);

            GameObject npcWalkRoadB = new GameObject("npcWalkRoadB");
            npcWalkRoadB.layer = 8;


            npcWalkRoadB.transform.parent = straightRoad.transform;
            npcWalkRoadB.AddComponent<MeshFilter>();
            npcWalkRoadB.AddComponent<MeshRenderer>();

            Mesh npcWalkRoadMeshB = new Mesh();
            Vector3[] npcWalkRoadMeshBVertices = new Vector3[vertices.Length];
            for (int i = 0; i < vertices.Length / 2; i++)
            {
                npcWalkRoadMeshBVertices[i * 2] = vertices[i * 2 + 1];
                npcWalkRoadMeshBVertices[i * 2 + 1] =
                    vertices[i * 2 + 1] + (vertices[i * 2 + 1] - vertices[i * 2]) * 0.3f;
            }

            npcWalkRoadMeshB.vertices = npcWalkRoadMeshBVertices;
            npcWalkRoadMeshB.triangles = triangles;
            npcWalkRoadMeshB.uv = uvs;

            npcWalkRoadB.GetComponent<MeshFilter>().mesh = npcWalkRoadMeshB;
            npcWalkRoadB.GetComponent<MeshRenderer>().material = npcWalkRoadMaterial;
            npcWalkRoadB.AddComponent<MeshCollider>();


            SpawnTurnSideCollider(npcWalkRoadB.transform, npcWalkRoadMeshBVertices, triangles, uvs, false);


            GameObject buildingsGround = new GameObject("buildingsGround");
            buildingsGround.transform.parent = straightRoad.transform;
            buildingsGround.AddComponent<MeshFilter>();
            buildingsGround.AddComponent<MeshRenderer>();

            Vector3[] buildingsGroundVertices = new Vector3[vertices.Length];

            for (int i = 0; i < vertices.Length / 2; i++)
            {
                buildingsGroundVertices[i * 2 + 1] = npcWalkRoadMeshVertices[i * 2];
                buildingsGroundVertices[i * 2] = npcWalkRoadMeshVertices[i * 2]
                                                 + (npcWalkRoadMeshVertices[i * 2] -
                                                    npcWalkRoadMeshVertices[i * 2 + 1]) * buildingsGroundWidth;
            }

            Mesh buildingsGroundMesh = new Mesh();
            buildingsGroundMesh.vertices = buildingsGroundVertices;
            buildingsGroundMesh.triangles = triangles;
            buildingsGroundMesh.uv = uvs;

            buildingsGround.GetComponent<MeshFilter>().mesh = buildingsGroundMesh;
            buildingsGround.GetComponent<MeshRenderer>().material = buildingsGroundMaterial;

            buildingsGround.AddComponent<MeshCollider>();


            GameObject buildingsGroundB = new GameObject("buildingsGround");
            buildingsGroundB.transform.parent = straightRoad.transform;
            buildingsGroundB.AddComponent<MeshFilter>();
            buildingsGroundB.AddComponent<MeshRenderer>();

            Vector3[] buildingsGroundVerticesB = new Vector3[vertices.Length];

            for (int i = 0; i < vertices.Length / 2; i++)
            {
                buildingsGroundVerticesB[i * 2] = npcWalkRoadMeshBVertices[i * 2 + 1];
                buildingsGroundVerticesB[i * 2 + 1] = npcWalkRoadMeshBVertices[i * 2 + 1]
                                                      + (npcWalkRoadMeshBVertices[i * 2 + 1] -
                                                         npcWalkRoadMeshBVertices[i * 2]) * buildingsGroundWidth;
            }

            Mesh buildingsGroundMeshB = new Mesh();
            buildingsGroundMeshB.vertices = buildingsGroundVerticesB;
            buildingsGroundMeshB.triangles = triangles;
            buildingsGroundMeshB.uv = uvs;

            buildingsGroundB.GetComponent<MeshFilter>().mesh = buildingsGroundMeshB;
            buildingsGroundB.GetComponent<MeshRenderer>().material = buildingsGroundMaterial;
            buildingsGroundB.AddComponent<MeshCollider>();

            Vector3 buildingPosA = buildingsGroundVertices[1];
            Vector3 buildingPosB = buildingsGroundVertices[3];
            Vector3 buildingsDir = (buildingsGroundVertices[1] - buildingsGroundVertices[0]).normalized;

            Vector3 buildingPosC = buildingsGroundVerticesB[0];
            Vector3 buildingPosD = buildingsGroundVerticesB[2];
            Vector3 buildingDirB = (buildingsGroundVerticesB[1] - buildingsGroundVerticesB[0]).normalized;


            int buildingsCount = 20;
            for (int i = 0; i < buildingsCount; i++)
            {
                for (int j = 0; j < 5; j++)
                {
                    float f = Random.Range(0.0f, 1.0f);

                    if (f < (float) 1 / (j + 1))
                    {
                        GameObject building = prefabToolBox.GetBuildingPrefab();
                        building.transform.position =
                            Vector3.Lerp(buildingPosA, buildingPosB, ((float) 1 / buildingsCount) * i);
                        building.transform.rotation = Quaternion.LookRotation(-buildingsDir);

                        building.transform.parent = straightRoad.transform;
                        building.transform.position += buildingsDir * (-4 + j * -6);

                        //  building.transform.position += transform.up * Random.Range(-4, 0);

                        building.transform.position +=
                            new Vector3(Random.Range(-1.0f, 1.0f) * 0.5f, Random.Range(-1.0f, 1.0f) * 0.5f,
                                Random.Range(-1.0f, 1.0f) * 0.5f);


                        building.transform.localScale = new Vector3(Random.Range(0.8f, 1.2f), Random.Range(0.8f, 1.8f),
                            Random.Range(0.8f, 1.2f));

                        building.transform.localScale *= Random.Range(0.9f, 1.1f);


                        building.transform.Rotate(new Vector3(0, f > 0.5f ? 180 : 0, 0));
                    }

                    float k = Random.Range(0.0f, 1.0f);
                    if (k < (float) 1 / (j + 1))
                    {
                        GameObject building = prefabToolBox.GetBuildingPrefab();
                        building.transform.position =
                            Vector3.Lerp(buildingPosC, buildingPosD, ((float) 1 / buildingsCount) * i);
                        building.transform.rotation = Quaternion.LookRotation(buildingsDir);


                        building.transform.parent = straightRoad.transform;
                        building.transform.position += buildingDirB * (4 + 6 * j);
                        //  building.transform.position += transform.up * Random.Range(-4, 0);
                        building.transform.position += new Vector3(
                            Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f) * 0.5f);
                        building.transform.localScale = new Vector3(
                            Random.Range(0.8f, 1.2f), Random.Range(0.8f, 1.8f), Random.Range(0.8f, 1.2f));
                        building.transform.localScale *= Random.Range(0.9f, 1.1f);
                        building.transform.Rotate(new Vector3(0, f > 0.5f ? 180 : 0, 0));
                    }
                }
            }

            int lightsCount = 2;
            for (int i = 0; i < lightsCount; i++)
            {
                GameObject streetLight = GameObject.Instantiate(prefabToolBox.streetLight_prefab, vertices[0],
                    Quaternion.LookRotation(vertices[2] - vertices[0]));
                streetLight.transform.position += streetLight.transform.forward * (i + 1) *
                                                  ((float) straightRoadLength / (lightsCount + 1));
                streetLight.transform.parent = straightRoad.transform;


                GameObject streetLightB = GameObject.Instantiate(prefabToolBox.streetLight_prefab, vertices[1],
                    Quaternion.LookRotation(vertices[2] - vertices[0]));
                streetLightB.transform.position += streetLightB.transform.forward * (i + 1) *
                                                   ((float) straightRoadLength / (lightsCount + 1));
                streetLightB.transform.parent = straightRoad.transform;
                streetLightB.transform.localScale = new Vector3(-1, 1, 1);
            }
        }

        public void SpawnTurnRoad()
        {
            GameObject turnRoad = new GameObject("turn_road" + roadSegIndex.ToString());

            gameModel.roadTurnSegs_list.Add(turnRoad);

            turnRoad.transform.parent = road_root;
            turnRoad.AddComponent<MeshFilter>();
            turnRoad.AddComponent<MeshRenderer>();

            int turnRoadSegCount = 10;
            Vector3[] vertices = new Vector3[turnRoadSegCount * 2];
            int[] triangles = new int[((vertices.Length / 2) - 1) * 6];
            Vector2[] uvs = new Vector2[vertices.Length];

            float segTurnAngle = Random.Range(-5.0f, 5.0f);


            for (int i = 0; i < vertices.Length / 2; i++)
            {
                vertices[2 * i] = roadBeginPos.position + roadBeginPos.right * -roadWidth / 2;
                vertices[2 * i + 1] = roadBeginPos.position + roadBeginPos.right * roadWidth / 2;
                roadBeginPos.Rotate(new Vector3(0, segTurnAngle, 0));
                roadBeginPos.position += roadBeginPos.forward * 2;
                AddRoadCenterPos(roadBeginPos.position, turnRoad.transform);
            }

            roadBeginPos.position += roadBeginPos.forward * -2;
            roadBeginPos.Rotate(new Vector3(0, -segTurnAngle, 0));
            AddRoadCenterPos(roadBeginPos.position, turnRoad.transform);
            int triangleIndex = 0;
            for (int i = 0; i < (vertices.Length / 2) - 1; i++)
            {
                triangles[triangleIndex] = 0 + 2 * i;
                triangles[triangleIndex + 1] = 2 + 2 * i;
                triangles[triangleIndex + 2] = 1 + 2 * i;

                triangles[triangleIndex + 3] = 1 + 2 * i;
                triangles[triangleIndex + 4] = 2 + 2 * i;
                triangles[triangleIndex + 5] = 3 + 2 * i;
                triangleIndex += 6;
            }

            for (int i = 0; i < turnRoadSegCount; i++)
            {
                uvs[2 * i] = new Vector2(0f, (float) i / 5);
                uvs[2 * i + 1] = new Vector2(1f, (float) i / 5);
            }


            Mesh turnRoadMesh = new Mesh();


            turnRoadMesh.vertices = vertices;
            turnRoadMesh.triangles = triangles;
            turnRoadMesh.uv = uvs;
            turnRoadMesh.RecalculateNormals();

            turnRoad.GetComponent<MeshFilter>().mesh = turnRoadMesh;
            turnRoad.GetComponent<MeshRenderer>().material = roadMaterial;
            turnRoad.AddComponent<MeshCollider>();


            GameObject npcWalkRoad = new GameObject("npcWalkRoad");
            npcWalkRoad.transform.parent = turnRoad.transform;

            npcWalkRoad.AddComponent<MeshFilter>();
            npcWalkRoad.AddComponent<MeshRenderer>();

            Mesh npcWalkRoadMesh = new Mesh();

            Vector3[] npcWalkRoadMeshVertices = new Vector3[vertices.Length];
            for (int i = 0; i < vertices.Length / 2; i++)
            {
                npcWalkRoadMeshVertices[i * 2 + 1] = vertices[i * 2];
                npcWalkRoadMeshVertices[i * 2] = vertices[i * 2] + (vertices[i * 2] - vertices[i * 2 + 1]) * 0.3f;
            }

            npcWalkRoadMesh.vertices = npcWalkRoadMeshVertices;
            npcWalkRoadMesh.triangles = triangles;
            npcWalkRoadMesh.uv = uvs;

            npcWalkRoad.GetComponent<MeshFilter>().mesh = npcWalkRoadMesh;
            npcWalkRoad.GetComponent<MeshRenderer>().material = npcWalkRoadMaterial;
            npcWalkRoad.AddComponent<MeshCollider>();

            SpawnTurnSideCollider(npcWalkRoad.transform, npcWalkRoadMeshVertices, triangles, uvs, true);


            GameObject npcWalkRoadB = new GameObject("npcWalkRoadB");
            npcWalkRoadB.transform.parent = turnRoad.transform;
            npcWalkRoadB.AddComponent<MeshFilter>();
            npcWalkRoadB.AddComponent<MeshRenderer>();

            Mesh npcWalkRoadMeshB = new Mesh();
            Vector3[] npcWalkRoadMeshBVertices = new Vector3[vertices.Length];
            for (int i = 0; i < vertices.Length / 2; i++)
            {
                npcWalkRoadMeshBVertices[i * 2] = vertices[i * 2 + 1];
                npcWalkRoadMeshBVertices[i * 2 + 1] =
                    vertices[i * 2 + 1] + (vertices[i * 2 + 1] - vertices[i * 2]) * 0.3f;
            }

            npcWalkRoadMeshB.vertices = npcWalkRoadMeshBVertices;
            npcWalkRoadMeshB.triangles = triangles;
            npcWalkRoadMeshB.uv = uvs;

            npcWalkRoadB.GetComponent<MeshFilter>().mesh = npcWalkRoadMeshB;
            npcWalkRoadB.GetComponent<MeshRenderer>().material = npcWalkRoadMaterial;
            npcWalkRoadB.AddComponent<MeshCollider>();

            SpawnTurnSideCollider(npcWalkRoadB.transform, npcWalkRoadMeshBVertices, triangles, uvs, false);


            GameObject buildingsGround = new GameObject("buildingsGround");
            buildingsGround.transform.parent = turnRoad.transform;
            buildingsGround.AddComponent<MeshFilter>();
            buildingsGround.AddComponent<MeshRenderer>();

            Vector3[] buildingsGroundVertices = new Vector3[vertices.Length];

            for (int i = 0; i < vertices.Length / 2; i++)
            {
                buildingsGroundVertices[i * 2 + 1] = npcWalkRoadMeshVertices[i * 2];
                buildingsGroundVertices[i * 2] = npcWalkRoadMeshVertices[i * 2]
                                                 + (npcWalkRoadMeshVertices[i * 2] -
                                                    npcWalkRoadMeshVertices[i * 2 + 1]) * buildingsGroundWidth;
            }

            Mesh buildingsGroundMesh = new Mesh();
            buildingsGroundMesh.vertices = buildingsGroundVertices;
            buildingsGroundMesh.triangles = triangles;
            buildingsGroundMesh.uv = uvs;

            buildingsGround.GetComponent<MeshFilter>().mesh = buildingsGroundMesh;
            buildingsGround.GetComponent<MeshRenderer>().material = buildingsGroundMaterial;

            GameObject buildingsGroundB = new GameObject("buildingsGround");
            buildingsGroundB.transform.parent = turnRoad.transform;
            buildingsGroundB.AddComponent<MeshFilter>();
            buildingsGroundB.AddComponent<MeshRenderer>();

            Vector3[] buildingsGroundVerticesB = new Vector3[vertices.Length];

            for (int i = 0; i < vertices.Length / 2; i++)
            {
                buildingsGroundVerticesB[i * 2] = npcWalkRoadMeshBVertices[i * 2 + 1];
                buildingsGroundVerticesB[i * 2 + 1] = npcWalkRoadMeshBVertices[i * 2 + 1]
                                                      + (npcWalkRoadMeshBVertices[i * 2 + 1] -
                                                         npcWalkRoadMeshBVertices[i * 2]) * buildingsGroundWidth;
            }

            Mesh buildingsGroundMeshB = new Mesh();
            buildingsGroundMeshB.vertices = buildingsGroundVerticesB;
            buildingsGroundMeshB.triangles = triangles;
            buildingsGroundMeshB.uv = uvs;

            buildingsGroundB.GetComponent<MeshFilter>().mesh = buildingsGroundMeshB;
            buildingsGroundB.GetComponent<MeshRenderer>().material = buildingsGroundMaterial;
        }


        public void AddRoadCenterPos(Vector3 spawnPos, Transform parentTransform)
        {
            GameObject roadCenterPos = new GameObject("roadCenterPos");
            roadCenterPos.transform.position = spawnPos;
            roadCenterPos.transform.parent = parentTransform;
            gameModel.roadCentrerPos_List.Add(roadCenterPos);
        }

        public void RemoveRoadBehind()
        {
            GameObject straightRoadRemoved = gameModel.roadStraightSegs_list[0];
            GameObject turnRoadRemoved = gameModel.roadTurnSegs_list[0];

            gameModel.roadStraightSegs_list.Remove(straightRoadRemoved);
            gameModel.roadTurnSegs_list.Remove(turnRoadRemoved);


            Transform[] children = straightRoadRemoved.GetComponentsInChildren<Transform>();
            foreach (Transform child in children)
            {
                gameModel.roadCentrerPos_List.Remove(child.gameObject);
            }

            Transform[] childrenB = turnRoadRemoved.GetComponentsInChildren<Transform>();
            foreach (Transform child in childrenB)
            {
                gameModel.roadCentrerPos_List.Remove(child.gameObject);
            }


            Destroy(straightRoadRemoved);
            Destroy(turnRoadRemoved);
        }


        public void SpawnBoys(Vector3 spawnPos, Vector3 boyDir, float spawnDistanceMax)
        {
            GameObject boysRoot = new GameObject("BoysRoot");

            boysRoot.transform.parent = road_root.transform;


            for (int i = 0; i < waveBoysCount; i++)
            {
                GameObject boy = GameObject.Instantiate(prefabToolBox.boyPrefab,
                    spawnPos, Quaternion.LookRotation(boyDir));
                boy.transform.position += boyDir * spawnDistanceMax * Random.Range(0.1f, 0.9f);

                float f = Random.Range(0.0f, 1.0f);
                boy.transform.position += boy.transform.right * Random.Range(roadWidth / 2, roadWidth / 2 + 4) *
                                          (f > 0.5f ? 1 : -1);
                boy.transform.parent = boysRoot.transform;
            }
        }


        public void SpawnObstacle(Vector3 spawnPos, Vector3 spawnDir, float enemyDistanceMax, Transform parentTransform)
        {
            GameObject obstacleRoot = new GameObject("ObstacleRoot");
            obstacleRoot.transform.parent = parentTransform;

            for (int i = 0; i < obstacleCount; i++)
            {
                Vector3 obstacleSpawnPos = spawnPos + spawnDir * Random.Range(0.0f, 1.0f) * enemyDistanceMax;
                GameObject obstacle = GameObject.Instantiate(prefabToolBox.obstaclePrefab, obstacleSpawnPos,
                    Quaternion.LookRotation(spawnDir));
                obstacle.transform.parent = obstacleRoot.transform;

                float f = Random.Range(0.0f, 1.0f);
                obstacle.transform.position += obstacle.transform.right *
                                               Random.Range(roadWidth / 2, roadWidth / 2 + 2) * (f > 0.5f ? 1 : -1);
            }
        }

        public void SpawnSideColliders(Transform parentTrans, Vector3[] vertices, int[] triangles, Vector2[] uvs)
        {
            GameObject roadColliderL = new GameObject("RoadColliderL");
            roadColliderL.transform.parent = parentTrans;

            roadColliderL.AddComponent<MeshFilter>();
            roadColliderL.AddComponent<MeshRenderer>();

            Mesh roadMeshL = new Mesh();

            Vector3[] colliderPoint =
            {
                vertices[0],
                vertices[2],
                vertices[0] + Vector3.up * 5,
                vertices[2] + Vector3.up * 5,
            };
            int[] colliderTriangles = triangles;
            Vector2[] colliderUvs = uvs;

            roadMeshL.vertices = colliderPoint;
            roadMeshL.triangles = colliderTriangles;
            roadMeshL.uv = colliderUvs;

            roadColliderL.GetComponent<MeshFilter>().mesh = roadMeshL;
            roadColliderL.GetComponent<MeshRenderer>().material = roadMaterial;
            roadColliderL.AddComponent<MeshCollider>();
            roadColliderL.AddComponent<Rigidbody>();
            roadColliderL.GetComponent<Rigidbody>().isKinematic = true;

            roadColliderL.GetComponent<MeshRenderer>().enabled = false;
        }

        public void SpawnTurnSideCollider(Transform parentTrans, Vector3[] vertices, int[] triangles, Vector2[] uvs,
            bool isLeftRoad)
        {
            GameObject roadCollider = new GameObject("RoadColliderL");
            roadCollider.transform.parent = parentTrans;

            roadCollider.AddComponent<MeshFilter>();
            roadCollider.AddComponent<MeshRenderer>();

            Mesh roadColliderMesh = new Mesh();
            Vector3[] roadColliderVerices = new Vector3[vertices.Length];

            for (int i = 0; i < roadColliderVerices.Length / 2; i++)
            {
                if (isLeftRoad)
                {
                    roadColliderVerices[2 * i] = vertices[2 * i] + Vector3.up * 5;
                    roadColliderVerices[2 * i + 1] = vertices[2 * i];
                }
                else
                {
                    roadColliderVerices[2 * i + 1] = vertices[2 * i + 1] + Vector3.up * 5;
                    roadColliderVerices[2 * i] = vertices[2 * i + 1];
                }
            }

            roadColliderMesh.vertices = roadColliderVerices;
            roadColliderMesh.triangles = triangles;
            roadColliderMesh.uv = uvs;

            roadCollider.GetComponent<MeshFilter>().mesh = roadColliderMesh;
            roadCollider.GetComponent<MeshRenderer>().material = roadMaterial;
            roadCollider.AddComponent<MeshCollider>();
            roadCollider.AddComponent<Rigidbody>();
            roadCollider.GetComponent<Rigidbody>().isKinematic = true;

            roadCollider.GetComponent<MeshRenderer>().enabled = false;
        }


        private void OnDrawGizmos()
        {
            if (gameModel == null) return;
            foreach (GameObject point in gameModel.roadCentrerPos_List)
            {
                Gizmos.color = Color.yellow;
                Gizmos.DrawSphere(point.transform.position, 0.4f);
            }
        }
    }
}