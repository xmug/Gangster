﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Crowd.Models;
using Crowd.PlayerEvents;
using UnityEngine;
using XMUGFramework;

namespace Crowd
{
    public class TeamManager : AManager
    {
        public Vector3[] teamPoints;
        public int linesCount;

        public float boysBetweenDistance;
        public float runSpeed;

        private List<GameObject> trails = new List<GameObject>();

        private PlayerModel playerModel;
        private GameModel gameModel;
        private EventSystem eventSystem;
        private TeamModel teamModel;
        private PrefabToolBox prefabToolBox;

        private void Awake()
        {
            //Enable gyro    
            Input.gyro.enabled = true; 
            
            playerModel = GameCore.Get<PlayerModel>();
            gameModel = GameCore.Get<GameModel>();
            eventSystem = GameCore.Get<EventSystem>();
            teamModel = GameCore.Get<TeamModel>();
            prefabToolBox = GameCore.Get<PrefabToolBox>();
            playerModel.runSpeed.Value = runSpeed;
            // Event
            eventSystem.Register<GetInvincibleStuffEvent>(param => { GetInvincibleStuff(); });

            //  Init

            int pointsCount = ((1 + linesCount) * linesCount) / 2;
            teamPoints = new Vector3[pointsCount];

            PlayerController[] players = FindObjectsOfType<PlayerController>();

            for (int i = 0; i < players.Length; i++)
            {
                if (players[i].inTeam)
                {
                    teamModel.playerList.Add(players[i]);
                }
            }
        }

        private void Update()
        {
            // GameOverDetect
            if (teamModel.playerList.Count <= 0)
            {
                if (gameModel.gameState.Value != GameState.GameOver)
                {
                    gameModel.gameState.Value = GameState.GameOver;
                }
            }

            if (gameModel.gameState.Value != GameState.GameStart)
            {
                return;
            }

            playerModel.playerCurRunDis += runSpeed * Time.deltaTime;

            teamModel.curLeader.Value = teamModel.playerList[0];
            if (!teamModel.curLeader.Value.isLeader)
            {
                teamModel.curLeader.Value.isLeader = true;
            }

            // Team Pos Update
            int pointIndex = 0;
            for (int j = 0; j < linesCount; j++)
            {
                for (int i = 0; i < linesCount; i++)
                {
                    if (i <= j)
                    {
                        teamPoints[pointIndex] = teamModel.curLeader.Value.transform.position;
                        teamPoints[pointIndex] += teamModel.curLeader.Value.transform.right * (-0.5f * j + i) *
                                                  boysBetweenDistance;
                        teamPoints[pointIndex] +=
                            teamModel.curLeader.Value.transform.forward * -j * boysBetweenDistance;
                        pointIndex++;
                    }
                }
            }


            for (int i = 1; i < teamModel.playerList.Count; i++)
            {
                //teamModel.playerList[i].transform.position = new Vector3(teamPoints[i].x, teamModel.playerList[i].transform.position.y, teamPoints[i].z);
                Vector3 targetPos = new Vector3(teamPoints[i].x, teamModel.playerList[i].transform.position.y,
                    teamPoints[i].z);
                teamModel.playerList[i].transform.position =
                    Vector3.Lerp(teamModel.playerList[i].transform.position, targetPos, 0.1f);

                teamModel.playerList[i].transform.rotation = Quaternion.Lerp(teamModel.playerList[i].transform.rotation,
                    teamModel.curLeader.Value.transform.rotation, 0.2f);
            }


            if (Input.GetKeyDown(KeyCode.J))
            {
                teamModel.curLeader.Value.Jump(0.0f);
                for (int i = 1; i < teamModel.playerList.Count; i++)
                {
                    float distanceToLeadBoy =
                        Vector3.Distance(teamModel.playerList[i].transform.position,
                            teamModel.curLeader.Value.transform.position);
                    teamModel.playerList[i].Jump(distanceToLeadBoy * 0.05f);
                }

                eventSystem.Trigger(new JumpEvent());
            }

            if (Input.touchCount > 0)
            {
                Touch touch = Input.GetTouch(0);
                if (touch.phase == TouchPhase.Began)
                {
                    teamModel.curLeader.Value.GetComponent<PlayerController>().Jump(0.0f);
                    for (int i = 1; i < teamModel.playerList.Count; i++)
                    {
                        float distanceToLeadBoy = Vector3.Distance(teamModel.playerList[i].transform.position,
                            teamModel.curLeader.Value.transform.position);
                        teamModel.playerList[i].Jump(distanceToLeadBoy * 0.05f);
                    }

                    eventSystem.Trigger(new JumpEvent());
                }
            }
            
            for (int i = 1; i < teamModel.playerList.Count; i++)
            {
                if (teamModel.playerList[i].transform.position.y < teamModel.curLeader.Value.transform.position.y - 10)
                {
                    teamModel.playerList[i].dead = true;
                    teamModel.playerList[i].inTeam = false;
                    teamModel.playerList.Remove(teamModel.playerList[i]);
                }
            }
        }

        public void GetInvincibleStuff()
        {
            teamModel.invincible = true;
            playerModel.runSpeed.Value += 35;
            foreach (PlayerController player in teamModel.playerList)
            {
                GameObject playerTrail =
                    GameObject.Instantiate(prefabToolBox.boyTrailPrefab,
                        player.transform.position + Vector3.up,
                        Quaternion.identity, player.transform);
                trails.Add(playerTrail);
            }

            Invoke("DisableInvincible", 5f);
        }

        public void DisableInvincible()
        {
            if (teamModel.invincible)
            {
                print(playerModel.runSpeed.Value);
                teamModel.invincible = false;
                playerModel.runSpeed.Value -= 35;
                print(playerModel.runSpeed.Value);

                foreach (GameObject trail in trails)
                {
                    DestroyImmediate(trail);
                }

                trails.Clear();
            }
        }

        private void OnDrawGizmos()
        {
            foreach (Vector3 point in teamPoints)
            {
                Gizmos.color = Color.green;
                Gizmos.DrawSphere(point, 0.2f);
            }
        }
    }
}