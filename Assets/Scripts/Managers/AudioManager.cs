﻿using System;
using Crowd.Models;
using Crowd.PlayerEvents;
using UnityEngine;
using XMUGFramework;
using Random = UnityEngine.Random;

namespace Crowd
{
    public class AudioManager : AManager
    {
        public AudioClip[] bloodClips;
        public AudioClip[] deadClips;
        public AudioClip[] getBoyClips;
        public AudioClip jumpClip;
        public AudioClip getMoneyClip;
        public AudioClip speedUpClip;
        
        private PlayerModel playerModel;
        private GameModel gameModel;
        private TeamModel teamModel;
        private EventSystem eventSystem;


        private void Awake()
        {
            playerModel = GameCore.Get<PlayerModel>();
            gameModel = GameCore.Get<GameModel>();
            teamModel = GameCore.Get<TeamModel>();
            eventSystem = GameCore.Get<EventSystem>();
            
            eventSystem.Register<JumpEvent>(param =>
            {
                JumpAudioPlay();
            });
            
            eventSystem.Register<GetBoyEvent>(param  => GetBoyAudioPlay());
            eventSystem.Register<GetMoneyEvent>(param  => GetMoneyAudioPlay());
            eventSystem.Register<BleedEvent>(param  => BloodAudioPlay());
            eventSystem.Register<DeathEvent>(param  => DeadAudioPlay());
            eventSystem.Register<SpeedUpEvent>(param  => SpeedUpAudioPlay());
        }

        public void BloodAudioPlay()
        {
            AudioSource.PlayClipAtPoint(bloodClips[Random.Range(0, bloodClips.Length)], transform.position);
        }

        public void DeadAudioPlay()
        {
            AudioSource.PlayClipAtPoint(deadClips[Random.Range(0, deadClips.Length)], transform.position);
        }


        public void GetBoyAudioPlay()
        {
            AudioSource.PlayClipAtPoint(getBoyClips[Random.Range(0, getBoyClips.Length)], transform.position);
        }

        public void JumpAudioPlay()
        {
            AudioSource.PlayClipAtPoint(jumpClip, transform.position);
        }

        public void GetMoneyAudioPlay()
        {
            AudioSource.PlayClipAtPoint(getMoneyClip, transform.position);
        }

        public void SpeedUpAudioPlay()
        {
            AudioSource.PlayClipAtPoint(speedUpClip, transform.position);
        }
    }
}