﻿using System.Collections.Generic;
using Crowd.ModelEvents;
using Crowd.Models;
using Crowd.PlayerEvents;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;
using UnityEngine.UI;
using XMUGFramework;

namespace Crowd
{
    public class UIManager : AManager
    {
        public Text distanceText;
        public float playerRunDistance;

        public Text levelMoneyText;
        public float levelMoneyCurrentCount;
        public int levelMeneyTargetCount;
        
        public Slider boysDefaultCountSlider;
        public Slider moneySpawnCountMaxSlider;
        public Slider charismaLevelSlider;


        public Text boysDefaultCountSliderText;
        public Text moneySpawnCountMaxText;
        public Text charismLevelText;

        public Text playerMoneyCountText;


        public Text playerDistanceText;
        public Text playerKillEnemiesCountText;
        public Text playerGetMoneyText;
        
        public enum CanvasEnum
        {
            defaultButtonsPanel,
            playerDataPanel,
            gamePlayingPanel,
            gameOverPanel,
            restartingGamePanel
        }

        public GameObject sceneChangeMask;
        public GameObject defaultButtonsPanel;
        public GameObject playerDatasPanel;
        public GameObject gamePlayingPanel;
        public GameObject gameOverPanel;

        public float panelChangeSpeed;


        public float screenWidth;
        public float screenHeight;


        public CanvasEnum canvasEnum;
        public Vector3 screenCenterPos;
        public Vector3 screenTopPos;

        private PrefabToolBox prefabToolBox;
        private PlayerModel playerModel;
        private GameModel gameModel;
        private TeamModel teamModel;
        private EventSystem eventSystem;


        private void Awake()
        {
            prefabToolBox = GameCore.Get<PrefabToolBox>();
            playerModel = GameCore.Get<PlayerModel>();
            gameModel = GameCore.Get<GameModel>();
            teamModel = GameCore.Get<TeamModel>();
            eventSystem = GameCore.Get<EventSystem>();

            gameModel.gameState.OnValueChanged += state =>
            {
                if (state == GameState.GameOver)
                {
                    CalculateResult();
                }
            };
            
            eventSystem.Register<GetMoneyEvent>(param =>
            {
                GetMoney(param.amount);
            });
        }

        private void Start()
        {
            UpdatePlayerData();
            screenWidth = Screen.width;
            screenHeight = Screen.height;

            screenCenterPos = new Vector3(screenWidth / 2, screenHeight / 2, 0);
            screenTopPos = new Vector3(screenWidth / 2, screenHeight * 2, 0);
        }

        private void Update()
        {
            if (gameModel.gameState.Value == GameState.GameStart)
            {
                distanceText.text = ((int) playerModel.playerCurRunDis).ToString() + "m";
            }

            // MoneyUp Animation
            if (levelMoneyCurrentCount < levelMeneyTargetCount)
            {
                levelMoneyCurrentCount += 0.3f;
            }

            levelMoneyText.text = "money: " + ((int) levelMoneyCurrentCount).ToString();
            PanelsController();
        }

        private void UpdatePlayerData()
        {
            boysDefaultCountSlider.value = playerModel.playerDafaultNum.Value;
            moneySpawnCountMaxSlider.value = playerModel.randomMoneyMax.Value;
            charismaLevelSlider.value = playerModel.joinProbability.Value * 10;

            boysDefaultCountSliderText.text = boysDefaultCountSlider.value.ToString();
            moneySpawnCountMaxText.text = moneySpawnCountMaxSlider.value.ToString();
            charismLevelText.text = charismaLevelSlider.value.ToString();

            playerMoneyCountText.text = "Balance ： " + playerModel.moneyCount.Value.ToString();
        }


        public void GetMoney(int moneyCount)
        {
            levelMeneyTargetCount += moneyCount;
        }

        public void RestartGameButtonClick()
        {
            //    GetComponent<Animator>().SetTrigger("ChangeScene");
            canvasEnum = CanvasEnum.restartingGamePanel;
            Invoke("ReloadScene", 1);
        }

        public List<GameObject> teamTrails;

        public void GameStartButtonClick()
        {
            gameModel.gameState.Value = GameState.GameStart;
        }


        public void ReloadScene()
        {
            Scene currentScene = SceneManager.GetActiveScene();
            SceneManager.LoadScene(currentScene.name);
        }


        public void QuitGame()
        {
            Application.Quit();
        }


        public void UpdateBoysCountButtonClick()
        {
            if (playerModel.moneyCount.Value >= 100)
            {
                playerModel.moneyCount.Value -= 100;
                playerModel.playerDafaultNum.Value++;
                UpdatePlayerData();
            }
        }

        public void UpdateSpawnMoneyButtonClick()
        {
            if (playerModel.moneyCount.Value >= 100)
            {
                playerModel.moneyCount.Value -= 100;
                playerModel.randomMoneyMax.Value++;
                UpdatePlayerData();
            }
        }

        public void UpdateCharismaLevelButtonClick()
        {
            if (playerModel.moneyCount.Value >= 100)
            {
                playerModel.moneyCount.Value -= 100;
                playerModel.joinProbability.Value++;
                UpdatePlayerData();
            }
        }


        public void CalculateResult()
        {
            canvasEnum = CanvasEnum.gameOverPanel;

            playerDistanceText.text = "Final Distance ：" + ((int) playerModel.playerCurRunDis).ToString();
            playerGetMoneyText.text = "Final Money ：" + ((int) levelMoneyCurrentCount).ToString();
            playerKillEnemiesCountText.text = "Enemy Killed ：" + ((int) playerModel.killEnemiesCount).ToString();

            playerModel.moneyCount.Value += (int) levelMoneyCurrentCount;
        }

        public void ToPlayerDataPanel()
        {
            canvasEnum = CanvasEnum.playerDataPanel;
        }

        public void ToGamePlayingPanel()
        {
            canvasEnum = CanvasEnum.gamePlayingPanel;
        }

        public void ToGameOverPanel()
        {
            canvasEnum = CanvasEnum.gameOverPanel;
        }

        public void ToDefaultPanel()
        {
            canvasEnum = CanvasEnum.defaultButtonsPanel;
        }


        public void PanelsController()
        {
            switch (canvasEnum)
            {
                case CanvasEnum.defaultButtonsPanel:

                    sceneChangeMask.transform.position = Vector3.Lerp(sceneChangeMask.transform.position, screenTopPos,
                        panelChangeSpeed);
                    defaultButtonsPanel.transform.position = Vector3.Lerp(defaultButtonsPanel.transform.position,
                        screenCenterPos, panelChangeSpeed);
                    playerDatasPanel.transform.position = Vector3.Lerp(playerDatasPanel.transform.position,
                        screenTopPos, panelChangeSpeed);
                    gamePlayingPanel.transform.position = Vector3.Lerp(gamePlayingPanel.transform.position,
                        screenTopPos, panelChangeSpeed);
                    gameOverPanel.transform.position = Vector3.Lerp(gameOverPanel.transform.position, screenTopPos,
                        panelChangeSpeed);

                    break;
                case CanvasEnum.playerDataPanel:
                    sceneChangeMask.transform.position = Vector3.Lerp(sceneChangeMask.transform.position, screenTopPos,
                        panelChangeSpeed);
                    defaultButtonsPanel.transform.position = Vector3.Lerp(defaultButtonsPanel.transform.position,
                        screenTopPos, panelChangeSpeed);
                    playerDatasPanel.transform.position = Vector3.Lerp(playerDatasPanel.transform.position,
                        screenCenterPos, panelChangeSpeed);
                    gamePlayingPanel.transform.position = Vector3.Lerp(gamePlayingPanel.transform.position,
                        screenTopPos, panelChangeSpeed);

                    gameOverPanel.transform.position = Vector3.Lerp(gameOverPanel.transform.position, screenTopPos,
                        panelChangeSpeed);

                    break;
                case CanvasEnum.gamePlayingPanel:

                    sceneChangeMask.transform.position = Vector3.Lerp(sceneChangeMask.transform.position, screenTopPos,
                        panelChangeSpeed);
                    defaultButtonsPanel.transform.position = Vector3.Lerp(defaultButtonsPanel.transform.position,
                        screenTopPos, panelChangeSpeed);
                    playerDatasPanel.transform.position = Vector3.Lerp(playerDatasPanel.transform.position,
                        screenTopPos, panelChangeSpeed);
                    gamePlayingPanel.transform.position = Vector3.Lerp(gamePlayingPanel.transform.position,
                        screenCenterPos, panelChangeSpeed);
                    gameOverPanel.transform.position = Vector3.Lerp(gameOverPanel.transform.position, screenTopPos,
                        panelChangeSpeed);

                    break;
                case CanvasEnum.gameOverPanel:
                    sceneChangeMask.transform.position = Vector3.Lerp(sceneChangeMask.transform.position, screenTopPos,
                        panelChangeSpeed);
                    gameOverPanel.transform.position = Vector3.Lerp(gameOverPanel.transform.position,
                        screenCenterPos, panelChangeSpeed);
                    defaultButtonsPanel.transform.position = Vector3.Lerp(defaultButtonsPanel.transform.position,
                        screenTopPos, panelChangeSpeed);
                    playerDatasPanel.transform.position = Vector3.Lerp(playerDatasPanel.transform.position,
                        screenTopPos, panelChangeSpeed);
                    gamePlayingPanel.transform.position = Vector3.Lerp(gamePlayingPanel.transform.position,
                        screenTopPos, panelChangeSpeed);


                    break;
                case CanvasEnum.restartingGamePanel:
                    sceneChangeMask.transform.position = Vector3.Lerp(sceneChangeMask.transform.position,
                        screenCenterPos, panelChangeSpeed);
                    sceneChangeMask.GetComponent<Image>().color =
                        Color.Lerp(sceneChangeMask.GetComponent<Image>().color, Color.black, panelChangeSpeed);
                    sceneChangeMask.transform.position = Vector3.Lerp(sceneChangeMask.transform.position,
                        screenCenterPos, panelChangeSpeed);


                    break;
                default:
                    break;
            }
        }
    }
}