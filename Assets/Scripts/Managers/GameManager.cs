﻿using UnityEngine;
using UnityEngine.SceneManagement;
using XMUGFramework;

namespace Crowd
{
    public class GameManager : AManager
    {
        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.R))
            {
                RestartGame();
            }

            if (Input.GetKeyDown(KeyCode.Escape))
            {
                Quit();
            }
        }

        public void RestartGame()
        {
            Scene currentScene = SceneManager.GetActiveScene();
            SceneManager.LoadScene(currentScene.name);
        }

        public void Quit()
        {
            Application.Quit();
        }
    }
}