﻿using UnityEngine;
using XMUGFramework;

namespace Crowd.PlayerEvents
{
    public struct JumpEvent : IEvent<JumpEvent>
    {
    }

    public struct HittedByEnemyEvent : IEvent<HittedByEnemyEvent>
    {
        public int hash;
    }

    public struct GetBoyEvent : IEvent<GetBoyEvent>
    {
    }

    public struct BleedEvent : IEvent<BleedEvent>
    {
    }

    public struct DeathEvent : IEvent<DeathEvent>
    {
    }

    public struct GetInvincibleStuffEvent : IEvent<GetInvincibleStuffEvent>
    {
    }
    
    public struct SpeedUpEvent : IEvent<SpeedUpEvent>
    {
    }
    
    public struct GetMoneyEvent : IEvent<GetMoneyEvent>
    {
        public int amount;
    }
}