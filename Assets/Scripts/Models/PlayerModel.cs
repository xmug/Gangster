﻿using UnityEngine;
using XMUGFramework;

namespace Crowd.Models
{
    public class PlayerModel : AModel
    {
        public BindableProperty<float> runSpeed = new BindableProperty<float>();
        public float playerCurRunDis = 0;
        public int killEnemiesCount = 0;
        
        public BindableProperty<int> moneyCount = new BindableProperty<int>();
        public BindableProperty<int> playerDafaultNum = new BindableProperty<int>();
        public BindableProperty<float> joinProbability = new BindableProperty<float>()
        {
            Value = 0.1f
        };
        public BindableProperty<float> randomMoneyMax = new BindableProperty<float>();

        public override void Init()
        {
            var storage = GameCore.Get<PlayerPrefsStorage>();
            var eSys = GameCore.Get<EventSystem>();
            
            // Load
            moneyCount.Value = (int) storage.GetValue("MoneyCount", 0f);
            playerDafaultNum.Value = (int) storage.GetValue("PlayerDafaultNum", 0f);
            joinProbability.Value = storage.GetValue("joinProbability", 0f);
            randomMoneyMax.Value = storage.GetValue("randomMoneyMax", 0f);
            
            // Save
            moneyCount.OnValueChanged += i => storage.SetValue("MoneyCount", i);
            playerDafaultNum.OnValueChanged += i => storage.SetValue("PlayerDafaultNum", i);
            joinProbability.OnValueChanged += i => storage.SetValue("JoinProbability", i);
            randomMoneyMax.OnValueChanged += i => storage.SetValue("RandomMoneyMax", i);
        }
    }
}