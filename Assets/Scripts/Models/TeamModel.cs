﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Reflection;
using XMUGFramework;

namespace Crowd.Models
{
    public class TeamModel : AModel
    {
        public bool invincible = false;
        
        public BindableProperty<PlayerController> curLeader = new BindableProperty<PlayerController>();
        public BindingList<PlayerController> playerList = new BindingList<PlayerController>();
        public override void Init()
        {
            
        }
    }
}