﻿using System.ComponentModel;
using UnityEngine;
using XMUGFramework;

namespace Crowd.Models
{
    public enum GameState
    {
        GamePause,
        GameStart,
        GameOver
    }
    
    public class GameModel : AModel
    {
        public BindableProperty<bool> isGameOver = new BindableProperty<bool>();
        public BindableProperty<GameState> gameState = new BindableProperty<GameState>();
        
        // MapModel
        public BindingList<GameObject> roadStraightSegs_list = new BindingList<GameObject>();
        public BindingList<GameObject> roadTurnSegs_list = new BindingList<GameObject>();
        public BindingList<GameObject> roadCentrerPos_List = new BindingList<GameObject>();

        public override void Init()
        {
            isGameOver.Value = false;
            gameState.Value = GameState.GamePause;
        }
    }
}