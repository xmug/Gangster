using Crowd.Models;
using Crowd.PlayerEvents;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;
using XMUGFramework;

namespace Crowd
{
    public class BossController : AController
    {
        public int segReachedIndex;
        public Vector3 targetDir;

        public float enemyLifeMax;
        public float enemyLifeCurrent;

        public Slider bossLifeSlider;

        public bool dead;
        public bool running;
        public float runSpeed;

        private Rigidbody rb;
        private Animator anim;


        private PrefabToolBox prefabToolBox;
        private PlayerModel playerModel;
        private GameModel gameModel;
        private TeamModel teamModel;
        private EventSystem eventSystem;


        private void Awake()
        {
            prefabToolBox = GameCore.Get<PrefabToolBox>();
            playerModel = GameCore.Get<PlayerModel>();
            gameModel = GameCore.Get<GameModel>();
            teamModel = GameCore.Get<TeamModel>();
            eventSystem = GameCore.Get<EventSystem>();

            rb = GetComponent<Rigidbody>();
            anim = GetComponent<Animator>();

            anim.SetFloat("Speed", 1);


            enemyLifeMax = teamModel.playerList.Count * 0.5f + 2;

            bossLifeSlider.maxValue = (int) enemyLifeMax;
            enemyLifeCurrent = enemyLifeMax;
            running = true;
        }


        void Start()
        {
            RandomSkin();
        }


        private void Update()
        {
            if (running)
            {
                if (Vector3.Distance(transform.position, teamModel.curLeader.Value.transform.position) < 15)
                {
                    targetDir = Vector3.Lerp(targetDir,
                        (teamModel.curLeader.Value.transform.position - transform.position).normalized, 0.08f);
                    targetDir = targetDir.normalized;
                }
                else
                {
                    ChechPos();
                }

                transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(targetDir), 0.2f);
                transform.position += transform.forward * runSpeed * Time.deltaTime;
            }

            anim.SetBool("Grounded", running);

            if (enemyLifeCurrent > 0)
            {
                bossLifeSlider.value = enemyLifeCurrent;
                bossLifeSlider.transform.rotation =
                    Quaternion.LookRotation(Camera.main.transform.position - bossLifeSlider.transform.position);
            }
        }

        public void ChechPos()
        {
            segReachedIndex = gameModel.roadCentrerPos_List.Count - 1;
            for (int i = gameModel.roadCentrerPos_List.Count - 1; i > 0; i--)
            {
                Vector3 dirA = transform.position - gameModel.roadCentrerPos_List[i].transform.position;
                if (Vector3.Dot(-dirA, transform.forward) < 0)
                {
                    targetDir = gameModel.roadCentrerPos_List[i - 1].transform.position -
                                gameModel.roadCentrerPos_List[i].transform.position;
                    targetDir = targetDir.normalized;

                    segReachedIndex = i;
                }
            }

            if (segReachedIndex <= 3 && segReachedIndex > 1)
            {
                Destroy(gameObject);
            }
        }

        public void Killed()
        {
            Destroy(bossLifeSlider.gameObject);

            running = false;

            for (int i = 0; i < 3; i++)
            {
                GameObject getMoneyEffect = GameObject.Instantiate(prefabToolBox.getMoneyEffectPrefab,
                    transform.position,
                    Quaternion.identity);
                Destroy(getMoneyEffect, 3);
            }


            rb.constraints = RigidbodyConstraints.None;
            anim.enabled = false;
        }


        public void RandomSkin()
        {
            SkinnedMeshRenderer[] skins = GetComponentsInChildren<SkinnedMeshRenderer>();

            foreach (SkinnedMeshRenderer skin in skins)
            {
                skin.enabled = false;
            }

            skins[Random.Range(0, skins.Length)].enabled = true;
        }

        public void HittedByPlayer()
        {
            enemyLifeCurrent--;
            bossLifeSlider.value = enemyLifeCurrent;

            if (enemyLifeCurrent <= 0)
            {
                if (!dead)
                {
                    dead = true;

                    Killed();
                }
            }
        }


        private void OnTriggerEnter(Collider other)
        {
            if (other.TryGetComponent<PlayerController>(out var playerController))
            {
                if (running)
                {
                    if (playerController.inTeam)
                    {
                        HittedByPlayer();

                        if (teamModel.invincible)
                        {
                            return;
                        }

                        playerController.HittedByEenmy();
                    }
                }
            }
        }
    }
}