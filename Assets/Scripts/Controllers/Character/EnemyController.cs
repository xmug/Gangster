using System.Collections;
using System.Collections.Generic;
using Crowd.Models;
using Crowd.PlayerEvents;
using UnityEngine;
using XMUGFramework;

namespace Crowd
{
    public class EnemyController : AController
    {
        private Vector3 targetDir;
        public float runSpeed;

        private Animator anim;
        private Rigidbody rb;
        public bool running;

        public int segReachedIndex;
        
        private PrefabToolBox prefabToolBox;
        private PlayerModel playerModel;
        private GameModel gameModel;
        private TeamModel teamModel;
        private EventSystem eventSystem;


        private void Awake()
        {
            prefabToolBox = GameCore.Get<PrefabToolBox>();
            playerModel = GameCore.Get<PlayerModel>();
            gameModel = GameCore.Get<GameModel>();
            teamModel = GameCore.Get<TeamModel>();
            eventSystem = GameCore.Get<EventSystem>();
            
            anim = GetComponent<Animator>();
            anim.SetFloat("Speed", 1);
            anim.SetBool("Grounded", true);
            anim.SetFloat("RunOffset", Random.Range(0.0f, 1.0f));
            RandomSkin();

            rb = GetComponent<Rigidbody>();
            running = true;
        }

        public void CheckPos()
        {
            segReachedIndex = gameModel.roadCentrerPos_List.Count - 1;
            for (int i = gameModel.roadCentrerPos_List.Count - 1; i > 0; i--)
            {
                Vector3 dirA = transform.position - gameModel.roadCentrerPos_List[i].transform.position;

                if (Vector3.Dot(-dirA, transform.forward) < 0)
                {
                    targetDir = gameModel.roadCentrerPos_List[i - 1].transform.position -
                                gameModel.roadCentrerPos_List[i].transform.position;
                    targetDir = targetDir.normalized;

                    segReachedIndex = i;
                }
            }

            if (segReachedIndex <= 3 && segReachedIndex > 1)
            {
                Destroy(gameObject);
            }
        }

        private void Update()
        {
            if (running)
            {
                if (Vector3.Distance(transform.position, teamModel.curLeader.Value.transform.position) < 15)
                {
                    targetDir = Vector3.Lerp(targetDir,
                        (teamModel.curLeader.Value.transform.position - transform.position).normalized, 0.08f);
                    targetDir = targetDir.normalized;
                }
                else
                {
                    CheckPos();
                }

                transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(targetDir), 0.2f);
                transform.position += transform.forward * runSpeed * Time.deltaTime;
            }
        }


        public void RandomSkin()
        {
            SkinnedMeshRenderer[] skins = GetComponentsInChildren<SkinnedMeshRenderer>();

            foreach (SkinnedMeshRenderer skin in skins)
            {
                skin.enabled = false;
            }

            skins[Random.Range(0, skins.Length)].enabled = true;
        }

        public void HittedByPlayer()
        {
            running = false;
            anim.SetFloat("Speed", 0);
            anim.enabled = false;
            rb.constraints = RigidbodyConstraints.None;
            rb.velocity = new Vector3(Random.Range(-7.0f, 7.0f), 20, Random.Range(-7.0f, 7.0f));

            GameObject getMoneyEffect = Instantiate(prefabToolBox.getMoneyEffectPrefab,
                transform.position + Vector3.up * 3.5f,
                Quaternion.LookRotation(transform.position - Camera.main.transform.position));
            Destroy(getMoneyEffect, 3);

            playerModel.killEnemiesCount++;

            eventSystem.Trigger(new BleedEvent());
            eventSystem.Trigger(new DeathEvent());
        }


        private void OnTriggerEnter(Collider other)
        {
            if (other.TryGetComponent<PlayerController>(out var playerController))
            {
                if (running)
                {
                    if (playerController.inTeam)
                    {
                        HittedByPlayer();

                        if (teamModel.invincible)
                        {
                            return;
                        }


                        playerController.HittedByEenmy();
                    }
                }
            }

            if (other.CompareTag("Obstacle"))
            {
                Destroy(this.gameObject);
            }
        }
    }
}