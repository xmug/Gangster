﻿using System.Collections.Generic;
using System.Numerics;
using Crowd.CamEvents;
using Crowd.Models;
using Crowd.PlayerEvents;
using Crowd.SpawnEvents;
using UnityEngine;
using XMUGFramework;
using Quaternion = UnityEngine.Quaternion;
using Random = UnityEngine.Random;
using Vector3 = UnityEngine.Vector3;

namespace Crowd
{
    public class PlayerController : AController
    {
        public Vector3 targetDir;

        private Animator anim;
        private Rigidbody rb;

        public float horizontal_input;
        public float horizontal_speed;

        public float jumpForce;
        public bool grounded;
        public LayerMask groundLayer;

        public bool inTeam;
        public bool isLeader;

        public bool dead;
        public bool runAway;

        // ObjectPool objectPool;

        public GameObject JoinIcon;

        public bool canJoin;
        public Vector3 randomRunAwayDir;

        private TeamModel teamModel;
        private GameModel gameModel;
        private PlayerModel playerModel;
        private EventSystem eventSystem;
        private PrefabToolBox prefabToolBox;

        private void Awake()
        {
            gameModel = GameCore.Get<GameModel>();
            eventSystem = GameCore.Get<EventSystem>();
            playerModel = GameCore.Get<PlayerModel>();
            teamModel = GameCore.Get<TeamModel>();
            prefabToolBox = GameCore.Get<PrefabToolBox>();
        }

        private void Start()
        {
            anim = GetComponent<Animator>();
            anim.SetBool("Grounded", true);
            anim.SetFloat("Speed", 1);
            anim.SetFloat("RunOffset", Random.Range(0.0f, 1.0f));

            rb = GetComponent<Rigidbody>();
            RandomSkin();

            float f = Random.Range(0.0f, 1.0f);
            if (f > playerModel.joinProbability.Value * 0.5f + 0.5f)
            {
                Destroy(JoinIcon);
                canJoin = false;
            }
            else
            {
                canJoin = true;
            }
        }

        private void FixedUpdate()
        {
            if (dead)
            {
                return;
            }

            GroundedCheck();

            if (isLeader)
            {
                CheckPos();
            }

            if (inTeam)
            {
                RotationCheck();
                Movement();
                GetNewBoy();
            }
            else
            {
                anim.SetFloat("Speed", 0);
                DestroyCheck();
            }

            if (gameModel.gameState.Value == GameState.GameStart)
            {
                anim.speed = (float) (gameModel.gameState.Value == GameState.GameStart
                    ? playerModel.runSpeed.Value / 15
                    : 0);
                if (inTeam)
                {
                    anim.SetFloat("Speed", 1);
                }
                else
                {
                    anim.SetFloat("Speed", 0);
                }
            }
            else
            {
                anim.SetFloat("Speed", 0);
            }

            CheckJoinIcon();
            if (runAway)
            {
                anim.SetFloat("Speed", 1);

                transform.rotation = Quaternion.LookRotation(randomRunAwayDir);
                rb.velocity =
                    transform.forward * playerModel.runSpeed.Value + Vector3.up * rb.velocity.y;
            }
        }

        public void CheckPos()
        {
            for (int i = 0; i < gameModel.roadCentrerPos_List.Count - 1; i++)
            {
                Vector3 dirA = transform.position - gameModel.roadCentrerPos_List[i].transform.position;
                if (Vector3.Dot(Camera.main.transform.forward, -dirA) < 0)
                {
                    targetDir = gameModel.roadCentrerPos_List[i + 1].transform.position -
                                gameModel.roadCentrerPos_List[i].transform.position;
                    targetDir = targetDir.normalized;

                    if (i > 24)
                    {
                        eventSystem.Trigger(new SpawnMap());
                    }
                }
            }
        }

        public void DestroyCheck()
        {
            int segIndex = gameModel.roadCentrerPos_List.Count - 1;
            for (int i = 0; i < gameModel.roadCentrerPos_List.Count - 1; i++)
            {
                Vector3 dirA = transform.position - gameModel.roadCentrerPos_List[i].transform.position;
                if (Vector3.Dot(transform.forward, -dirA) < 0)
                {
                    segIndex = i;
                }
            }

            if (segIndex <= 3)
            {
                Destroy(gameObject);
            }
        }

        public void RotationCheck()
        {
            if (isLeader)
            {
                if (targetDir.magnitude > 0)
                {
                    horizontal_input = Input.GetAxis("Horizontal");
                    
                    // gyro
                    if (Input.gyro.attitude != Quaternion.identity)
                    {
                        horizontal_input = Input.gyro.attitude.y;
                    }
                    Vector3 dirAdditional = Camera.main.transform.right *
                                            (horizontal_input + Input.acceleration.x * 3.5f) * 0.75f;
                    targetDir = (targetDir + dirAdditional).normalized;

                    transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(targetDir), 0.25f);
                }
            }
            else
            {
                if (teamModel.curLeader.Value != null)
                {
                    targetDir = teamModel.curLeader.Value.transform.forward;
                }
            }
        }

        public void CheckJoinIcon()
        {
            if (inTeam)
            {
                if (JoinIcon != null)
                {
                    Destroy(JoinIcon);
                }
            }
        }


        public void Movement()
        {
            anim.SetFloat("Speed", 1);
            rb.velocity =
                targetDir.normalized * playerModel.runSpeed.Value + Vector3.up * rb.velocity.y;

            // extra gravity
            rb.velocity -= new Vector3(0, 0.9f, 0);

            if (targetDir.magnitude > 0)
            {
                transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(targetDir), 0.25f);
            }
        }

        public void GetNewBoy()
        {
            var castInfos = Physics.BoxCastAll(transform.position, Vector3.one, transform.forward);
            foreach (var info in castInfos)
            {
                if (info.transform.TryGetComponent<PlayerController>(out PlayerController player))
                {
                    if (Vector3.Distance(transform.position, player.transform.position) < 2)
                    {
                        if (!player.inTeam && !player.dead)
                        {
                            if (player.canJoin)
                            {
                                player.inTeam = true;
                                teamModel.playerList.Add(player);
                                eventSystem.Trigger(new GetBoyEvent());
                            }
                            else
                            {
                                if (!player.runAway)
                                {
                                    player.runAway = true;
                                    player.RunAway();
                                }
                            }
                        }
                    }
                }
            }
        }


        public void Jump(float jumpDelay)
        {
            Invoke("JumpUp", jumpDelay + Random.Range(-0.1f, 0.1f));
        }

        public void JumpUp()
        {
            if (grounded)
            {
                rb.AddForce(transform.up * jumpForce, ForceMode.Impulse);
            }
        }


        private void OnDrawGizmos()
        {
            Gizmos.color = Color.green;
            Gizmos.DrawLine(transform.position, transform.position + targetDir * 5);
        }

        public void GroundedCheck()
        {
            Ray downRay = new Ray(transform.position + Vector3.up * 0.5f, Vector3.down);
            RaycastHit downHit;

            Physics.Raycast(downRay, out downHit, 1, groundLayer);

            if (downHit.collider && transform.position.y - downHit.point.y < 1)
            {
                grounded = true;
            }
            else
            {
                grounded = false;
            }

            anim.SetBool("Grounded", grounded);
        }

        public void RandomSkin()
        {
            SkinnedMeshRenderer[] skins = GetComponentsInChildren<SkinnedMeshRenderer>();

            foreach (SkinnedMeshRenderer skin in skins)
            {
                skin.enabled = false;
            }

            skins[Random.Range(0, skins.Length)].enabled = true;
        }


        public void HittedByEenmy()
        {
            if (inTeam)
            {
                inTeam = false;
                dead = true;
                anim.enabled = false;
                rb.constraints = RigidbodyConstraints.None;
                rb.velocity = new Vector3(Random.Range(-10, 10), 20, Random.Range(-10, 10));
                teamModel.playerList.Remove(gameObject.GetComponent<PlayerController>());
                eventSystem.Trigger(new CamShakingEvent());

                GameObject hitEffect =
                    GameObject.Instantiate(prefabToolBox.hitEffectPrefab, transform.position, Quaternion.identity);
                hitEffect.transform.position += Vector3.up * 0.5f;
                hitEffect.transform.position += transform.forward * 1.5f;
                Destroy(hitEffect, 2);


                GameObject bloodEffect = GameObject.Instantiate(prefabToolBox.bloodEffectPrefab,
                    transform.position + transform.forward * 1.5f + Vector3.up * 2, transform.rotation);
                Destroy(bloodEffect, 5);


                eventSystem.Trigger(new BleedEvent());
                eventSystem.Trigger(new DeathEvent());
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.CompareTag("Obstacle"))
            {
                if (!teamModel.invincible)
                {
                    HittedByEenmy();
                }
            }

            // if (other.gameObject.name == "Chicken_stuff(Clone)")
            // {
            //     eventSystem.Trigger(new GetInvincibleStuffEvent());
            //     Destroy(other.gameObject);
            //     eventSystem.Trigger(new SpeedUpEvent());
            // }

            if (other.gameObject.name == "PlayersReachedLine(Clone)")
            {
                if (!teamModel.invincible)
                {
                    eventSystem.Trigger(new GetInvincibleStuffEvent());
                    Destroy(other.gameObject, 2);
                    eventSystem.Trigger(new SpeedUpEvent());
                }
            }
        }

        public void RunAway()
        {
            float g = Random.Range(-1.0f, 1.0f);
            randomRunAwayDir = (transform.right * g + transform.forward * Mathf.Abs(0.5f * g)).normalized;
            GetComponent<CapsuleCollider>().enabled = false;
            rb.useGravity = false;
            Destroy(gameObject, 5);
        }
    }
}