using System.Collections;
using System.Collections.Generic;
using Crowd.PlayerEvents;
using UnityEngine;
using XMUGFramework;

namespace Crowd
{
    public class MoneyController : MonoBehaviour
    {
        private PrefabToolBox prefabToolBox;
        private EventSystem eventSystem;

        private void Awake()
        {
            prefabToolBox = GameCore.Get<PrefabToolBox>();
            eventSystem = GameCore.Get<EventSystem>();
        }

        private void Update()
        {
            transform.Rotate(Vector3.up * Time.deltaTime * 5);
            transform.position += Vector3.up * (Mathf.Cos(Time.time) * 0.01f);
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.GetComponent<PlayerController>())
            {
                if (other.GetComponent<PlayerController>().inTeam)
                {
                    Destroy(gameObject);
                    GameObject getMoneyEffect = Instantiate(prefabToolBox.getMoneyEffectPrefab,
                        transform.position, Quaternion.identity);
                }
            }
        }
    }
}
