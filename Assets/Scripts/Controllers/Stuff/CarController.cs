using Crowd.Models;
using Crowd.PlayerEvents;
using UnityEngine;
using XMUGFramework;

namespace Crowd
{
    public class CarController : MonoBehaviour
    {
        Vector3 targetDir;
        public float speed;
        public int segReachedIndex;
        private GameModel gameModel;

        private void Awake()
        {
            gameModel = GameCore.Get<GameModel>();
        }

        private void Update()
        {
            CheckDir();
            transform.position += speed * Time.deltaTime * transform.forward;
        }


        public void CheckDir()
        {
            segReachedIndex = gameModel.roadCentrerPos_List.Count - 1;
            for (int i = gameModel.roadCentrerPos_List.Count - 1; i > 0; i--)
            {
                Vector3 dirA = transform.position - gameModel.roadCentrerPos_List[i].transform.position;
                if (Vector3.Dot(-dirA, transform.forward) < 0)
                {
                    targetDir = gameModel.roadCentrerPos_List[i - 1].transform.position -
                                gameModel.roadCentrerPos_List[i].transform.position;
                    targetDir = targetDir.normalized;
                    segReachedIndex = i;
                }
            }

            if (segReachedIndex <= 3 && segReachedIndex > 1)
            {
                Destroy(gameObject);
            }

            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(targetDir), 0.2f);
        }


        private void OnTriggerEnter(Collider other)
        {
            if (other.TryGetComponent<PlayerController>(out var playerController))
            {
                if (playerController.inTeam)
                {
                    if (GameCore.Get<TeamModel>().invincible)
                    {
                        return;
                    }
                    playerController.HittedByEenmy();
                }
            }
        }
    }
}