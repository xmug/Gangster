using System.Collections;
using System.Collections.Generic;
using Crowd;
using Crowd.Models;
using Crowd.PlayerEvents;
using UnityEngine;
using XMUGFramework;

public class GetMoneyEffect : MonoBehaviour
{
    public GameObject textObject;

    private void Awake()
    {
        int randomMoneyCount = GetRandomMoney();
        GameCore.Get<EventSystem>().Trigger(new GetMoneyEvent()
        {
            amount = randomMoneyCount
        });

        textObject.GetComponent<TextMesh>().text = "+ " + randomMoneyCount.ToString();
    }

    public int GetRandomMoney()
    {
        var tmp = GameCore.Get<PlayerModel>().randomMoneyMax.Value;
        int moneyCount = (int) Random.Range(tmp * 0.6f, tmp) + 3;
        return moneyCount;
    }

    private void Update()
    {
        textObject.transform.localScale = Vector3.Lerp(textObject.transform.localScale, Vector3.one * 0.05f, 0.1f);
        transform.position += Vector3.up * Time.deltaTime * 2;
    }
}