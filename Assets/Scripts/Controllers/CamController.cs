﻿using Crowd.Models;
using UnityEngine;
using XMUGFramework;
using Random = UnityEngine.Random;

namespace Crowd
{
    public class CamController : AController
    {
        private PlayerController player;
        private Vector3 offset;

        public float camDistance;
        public float angle;

        private PlayerModel playerModel;
        private GameModel gameModel;
        private EventSystem eventSystem;
        private TeamModel teamModel;

        public Vector3 segDir;
        private float shakingTimer;

        private void Awake()
        {
            gameModel = GameCore.Get<GameModel>();
            eventSystem = GameCore.Get<EventSystem>();
            teamModel = GameCore.Get<TeamModel>();

            eventSystem.Register<CamEvents.CamShakingEvent>(CamShake);
        }

        private void Start()
        {
            if (teamModel.curLeader.Value != null)
            {
                offset = transform.position - teamModel.curLeader.Value.transform.position;
            }
        }

        private void FixedUpdate()
        {
            if (gameModel.isGameOver.Value)
            {
                return;
            }

            if (teamModel.curLeader.Value != null)
            {
                Transform leadPlayer = teamModel.curLeader.Value.transform;
                Vector3 leadBoyForwardPos = teamModel.curLeader.Value.transform.position +
                                            teamModel.curLeader.Value.transform.forward * 5;
            
                for (int i = 0; i < gameModel.roadCentrerPos_List.Count - 1; i++)
                {
                    Vector3 dirA = gameModel.roadCentrerPos_List[i].transform.position - leadBoyForwardPos;
            
                    if (Vector3.Dot(dirA, transform.forward) < 0)
                    {
                        segDir = gameModel.roadCentrerPos_List[i + 1].transform.position -
                                 gameModel.roadCentrerPos_List[i].transform.position;
                        segDir = segDir.normalized;
                    }
                }
            
                Vector3 targetPos =
                    leadBoyForwardPos + Vector3.up * Mathf.Sin((float) angle / 90 * Mathf.PI * 0.5f) * camDistance
                                      + transform.forward * -Mathf.Cos((float) angle / 90 * Mathf.PI * 0.5f) *
                                      camDistance;
            
            
                transform.position = Vector3.Lerp(transform.position, targetPos, 0.05f);
            
            
                Quaternion targetRotation = Quaternion.LookRotation(segDir + Vector3.down * 0.55f);
                transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, 0.08f);
            }
        }

        private void CamShake(CamEvents.CamShakingEvent param)
        {
            // 慢动作
            TimeSlowDown();
            
            StartCoroutine(FastEnumerator.TimerInvoke(
                curTimer =>
                {
                    transform.position += new Vector3(Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f),
                        Random.Range(-1.0f, 1.0f)) * curTimer;
                },
                shakingTimer));
        }
        
        public void TimeSlowDown()
        {
            Time.timeScale = 0.5f;
            Invoke("TimeBackNormal", 0.2f);
        }
        
        public void TimeBackNormal()
        {
            Time.timeScale = 1;
        }
    }
}